# JZJCoreD

NOTE: This core is abandoned in favour of a seperate naming scheme for pipelined JZJCore cpus: JZJPipelinedCoreX. The spirit of this core lives on in JZJCoreD

My 4th RISC-V cpu implementation with pipelining (RV32I)!

This cpu is largely different then JZJCoreA-C in terms of code (not just a fork). It will be my first pipelined cpu (probably 5 stages) and so I will be focusing on completion rather than performance (like the focus for JZJCoreA). JZJCoreE will be when I start focusing more on performance by better balancing combinational logic stages of the pipeline, doing branch prediction and speculative execution and other fancy stuff. For now, I fully expect JZJCoreD to be slower than the very optimized JZJCoreC because of pipeline stalls (though potentially a higher fmax if my pipeline divisions are semidecent). JZJCoreE will be the performance catch up core and by JZJCoreF I should definitely be outperforming JZJCoreC.

Like other cores, my goal is to make this cpu compatible with the standard JZJCore memory layout (it should not be too difficult). Zifencei might be a problem though so I don't know if that will be supported.

# Cycle counts for instructions

No idea how to measure this or an alternative metric for performance.

## Memory Map

Note: addresses are inclusive, bounds not specified are not mapped to anything, and execution starts at 0x00000000

| Bytewise Address (whole word) | Physical Word-wise Address | Function |
|:------------------------------|:---------------------------|:---------|
|0x00000000 to 0x00000003|0x00000000|RAM Start|
|0x0000FFFC to 0x0000FFFF|0x00003FFF|RAM End|
|0xFFFFFFE0 to 0xFFFFFFE3|0x3FFFFFF8|Memory Mapped IO Registers Start|
|0xFFFFFFFC to 0xFFFFFFFF|0x3FFFFFFF|Memory Mapped IO Registers End|
