module JZJCoreD//Thank you https://en.wikipedia.org/wiki/Classic_RISC_pipeline
#(
	parameter INITIAL_MEM_CONTENTS = "initialRam.mem",//File containing initial ram contents (32 bit words); execution starts from address 0x00000000
	parameter RAM_A_WIDTH = 12//number of addresses for code/ram (not memory mapped io); 2^RAM_A_WIDTH words = 2^RAM_A_WIDTH * 4 bytes
)
(
	input clock,
	input reset,
	
	//CPU memory mapped io ports (for io dir, 0 = read, 1 = write)
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portX[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portX[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Addresses for port read/write | io direction (0 for input, 1 for output)
	inout [31:0] portA,//FFFFFFF0   |   FFFFFFE0
	inout [31:0] portB,//FFFFFFF4   |   FFFFFFE4
	inout [31:0] portC,//FFFFFFF8   |   FFFFFFE8
	inout [31:0] portD,//FFFFFFFC   |   FFFFFFEC
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDirection[y] is 1 then that pin is outputting data, if portXDirection[y] is 0 then that pin is high impedance
	output [31:0] portADirection,
	output [31:0] portBDirection,
	output [31:0] portCDirection,
	output [31:0] portDDirection,
	
	//Output for legacy asembly test programs that output to register 31; for new software use memory mapped io instead
	output [31:0] register31Output
);
/* Wires, Registers, and Assignments */

//Stage 1
//Inputs
wire stage1Stall, stage1Annul;
wire [31:0] stage1InstructionIn;
wire [31:0] stage1NewPC;
//Outputs
wire [29:0] stage1InstructionMemoryAddress;
wire pcMisalignedErrorFlag;

//Stage 2
//Inputs
wire stage2Stall, stage2Annul;
wire [31:0] stage2InstructionIn;
wire [31:0] stage2PCIn;
wire [31:0] stage2RS1In;
wire [31:0] stage2RS2In;
//Outputs
wire [4:0] stage2RS1Address;
wire [4:0] stage2RS2Address;

//Stage 3
//Inputs
wire stage3Stall, stage3Annul;
wire [31:0] stage3RS1In;
wire [31:0] stage3RS2In;
wire [31:0] stage3ImmediateIn;
wire [31:0] stage3PCIn;
wire [6:0] stage3OpcodeIn;
wire [2:0] stage3Funct3In;
wire [6:0] stage3Funct7In;
wire [4:0] stage3RDAddressIn;
wire stage3MemoryReadFlagIn;
wire stage3MemoryWriteFlagIn;//1 for memory write, 0 for register write
//Outputs
wire [31:0] stage3NewPC;//Next value for the pc; yes this means that the cpu stalls for 2 cycles after every instruction, but that's ok: I'll improve cpi in later versions

//Stage 4
//Inputs
//Outputs

//Stage 5
//Inputs
//Outputs

//Things for stage independent modules 
//Register file//Todo move to the proper stages
wire registerFileWE;
wire [31:0] registerFileIn;
wire [4:0] registerFileRDAddress;
//Memory backend//Todo move to the proper stages
wire memoryBackendWE;
wire [29:0] memoryBackendAddress;
wire [31:0] memoryBackendDataIn;
wire [31:0] memoryBackendDataOut;
//Control logic//Todo move to the proper stages

/* Pipeline Stage Modules */

//Instruction fetch
Stage1 stage1 (.clock(clock), .reset(reset), .stall(stage1Stall), .annul(stage1Annul), .instructionIn(stage1InstructionIn), .pcIn(stage1NewPC), .instructionOut(stage2InstructionIn),
					.instructionAddress(stage1InstructionMemoryAddress), .pcOut(stage2PCIn), .pcMisaligned(pcMisalignedErrorFlag));
					
//Decode
Stage2 stage2 (.clock(clock), .reset(reset), .stall(stage2Stall), .annul(stage2Annul), .instructionIn(stage2InstructionIn), .pcIn(stage2PCIn), .rs1In(stage2RS1In), .rs2In(stage2RS2In),
					.rs1Address(stage2RS1Address), .rs2Address(stage2RS2Address), .rs1Out(stage3RS1In), .rs2Out(stage3RS2In), .immediateOut(stage3ImmediateIn), .pcOut(stage3PCIn),
					.opcodeOut(stage3OpcodeIn), .funct3Out(stage3Funct3In), .funct7Out(stage3Funct7In), .rdAddressOut(stage3RDAddressIn), .memoryReadFlagOut(stage3MemoryReadFlagIn),
					.memoryWriteFlagOut(stage3MemoryWriteFlagIn));

/* Stage Independent Modules */

//Register file
RegisterFile registerFile (.clock(clock), .reset(reset), .dataIn(registerFileIn), .rd(registerFileRDAddress), .writeEnable(registerFileWE), .rs1(stage2RS1Address), .rs2(stage2RS2Address),
									.portRS1(stage2RS1In), .portRS2(stage2RS2In), .register31Output(register31Output));

//Memory backend
MemoryBackend #(.INITIAL_RAM_CONTENTS(INITIAL_MEM_CONTENTS), .A_WIDTH(RAM_A_WIDTH)) memoryBackend
					(.clock(clock), .reset(reset), .address(memoryBackendAddress), .dataIn(memoryBackendDataIn), .writeEnable(memoryBackendWE), .dataOut(memoryBackendDataOut),
					 .instructionOut(stage1InstructionIn), .instructionAddress(stage1InstructionMemoryAddress), .mmPortA(portA), .mmPortB(portB),.mmPortC(portC), .mmPortD(portD),
					 .mmPortADirection(portADirection), .mmPortBDirection(portBDirection), .mmPortCDirection(portCDirection), .mmPortDDirection(portDDirection));								

endmodule