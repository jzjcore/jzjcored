module MemoryMappedIOManager
(
	input clock,
	input reset,
	
	//Addressing
	input [29:0] address,
	
	//IO
	input [31:0] dataIn,
	output [31:0] dataOut,
	input writeEnable,
	
	//CPU memory mapped rw ports (for io dir, 0 = read, 1 = write)
	//Word-wise addresses:						read/wri   |   io dir
	inout [31:0] mmPortA,//portArray[0]:	3FFFFFFC   |   3FFFFFF8
	inout [31:0] mmPortB,//portArray[1]:	3FFFFFFD   |   3FFFFFF9
	inout [31:0] mmPortC,//portArray[2]:	3FFFFFFE   |   3FFFFFFA
	inout [31:0] mmPortD,//portArray[3]:	3FFFFFFF   |   3FFFFFFB
	//inout reg [31:0] portArray [1:0]//Register 0 is portA read/write register, register 1 is portB read/write register, and so on//oops arrays need systemverilog extentions
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDir[y] is 1 then that pin is outputting data, if portXDir[y] is 0 then that pin is high impedance
	output [31:0] mmPortADirection,
	output [31:0] mmPortBDirection,
	output [31:0] mmPortCDirection,
	output [31:0] mmPortDDirection
);
/* Wires, Registers, and Assignments */

reg [31:0] ioDirectionRegisterBank [3:0];//Register 0 is portA io direction, register 1 is portB io direction, and so on
assign mmPortADirection = ioDirectionRegisterBank[0];
assign mmPortBDirection = ioDirectionRegisterBank[1];
assign mmPortCDirection = ioDirectionRegisterBank[2];
assign mmPortDDirection = ioDirectionRegisterBank[3];
reg [31:0] portA;
reg [31:0] portB;
reg [31:0] portC;
reg [31:0] portD;
assign mmPortA = updatedPortRegister(portA, ioDirectionRegisterBank[0]);//Take into account io direction for each bit
assign mmPortB = updatedPortRegister(portB, ioDirectionRegisterBank[1]);//Take into account io direction for each bit
assign mmPortC = updatedPortRegister(portC, ioDirectionRegisterBank[2]);//Take into account io direction for each bit
assign mmPortD = updatedPortRegister(portD, ioDirectionRegisterBank[3]);//Take into account io direction for each bit

wire [31:0] bigEndianDataIn = toBigEndian(dataIn);
reg [31:0] bigEndianDataOut;
assign dataOut = toLittleEndian(bigEndianDataOut);//Output logic will output 0 if not in bounds

/* Output Logic */

always @*
begin
	case (address)
		30'h3FFFFFF8: bigEndianDataOut = ioDirectionRegisterBank[0];
		30'h3FFFFFF9: bigEndianDataOut = ioDirectionRegisterBank[1];
		30'h3FFFFFFA: bigEndianDataOut = ioDirectionRegisterBank[2];
		30'h3FFFFFFB: bigEndianDataOut = ioDirectionRegisterBank[3];
		30'h3FFFFFFC: bigEndianDataOut = portA;
		30'h3FFFFFFD: bigEndianDataOut = portB;
		30'h3FFFFFFE: bigEndianDataOut = portC;
		30'h3FFFFFFF: bigEndianDataOut = portD;
		default: bigEndianDataOut = 32'h00000000;//not in bounds, don't output anything
	endcase
end

/* Writing Logic */
always @(posedge clock, posedge reset)
begin
	if (reset)
		initRegisters();
	else if (clock)
	begin
		if (writeEnable)
		begin
			case (address)
				30'h3FFFFFF8: ioDirectionRegisterBank[0] <= bigEndianDataIn;//portA io direction register
				30'h3FFFFFF9: ioDirectionRegisterBank[1] <= bigEndianDataIn;//portB io direction register
				30'h3FFFFFFA: ioDirectionRegisterBank[2] <= bigEndianDataIn;//portC io direction register
				30'h3FFFFFFB: ioDirectionRegisterBank[3] <= bigEndianDataIn;//portD io direction register
				30'h3FFFFFFC: portA <= bigEndianDataIn;//Update port A
				30'h3FFFFFFD: portB <= bigEndianDataIn;//Update port B
				30'h3FFFFFFE: portC <= bigEndianDataIn;//Update port C
				30'h3FFFFFFF: portD <= bigEndianDataIn;//Update port D
				default:
				begin
					//do nothing because address is not in bounds
				end
			endcase
		end
	end
end

/* Functions */

//Enable/disable individual bits output in mmPortX based on ioDirectionRegister[X] (changes to z)
function automatic [31:0] updatedPortRegister(input [31:0] rawPortRegister, input [31:0] ioDirectionRegister);//If a bit in the io dir register is 0, then the corresponding bit in updatedPortRegister is set to z. Otherwise it is passed through
integer i;
begin
	for (i = 0; i < 32; i = i + 1)
	begin
        updatedPortRegister[i] = ioDirectionRegister[i] ? rawPortRegister[i] : 1'bz;//If the ioDirectionRegister is set to 1, then we are writing the data in rawPortRegister, otherwise we are reading by first setting the register to z
	end
end
endfunction

//Endianness functions
`include "JZJCoreD/Memory/EndiannessFunctions.v"

/* Register Initialization */

initial
begin
	initRegisters();
end

//Set all io dir registers to 0 and set all port registers to z
task initRegisters();//Intentionally not automatic because the only things that use it are the initial block and reset inside of the write block
integer i;
begin
	//Set all io dir registers to 0
	for (i = 0; i < 4; i = i + 1)
	begin
		ioDirectionRegisterBank[i] <= 32'h00000000;
	end
	
	//Zero all port registers
	portA <= 32'h00000000;
	portB <= 32'h00000000;
	portC <= 32'h00000000;
	portD <= 32'h00000000;
end
endtask

endmodule