module MemoryBackend//Contains addressing logic for MemoryController to choose between various memory backends
#(
	parameter INITIAL_RAM_CONTENTS = "initialRam.mem",
	parameter A_WIDTH = 12
)
(//Note: addresses are only 30 bits because they are wordwise addresses
	input clock,
	input reset,
	
	//Addressing
	input [29:0] address,
	
	//Memory stores
	input [31:0] dataIn,
	input writeEnable,
	
	//Memory loads
	output [31:0] dataOut,
	
	//Instruction stuff
	output [31:0] instructionOut,
	input [29:0] instructionAddress,
	
	//CPU memory mapped io ports (for io dir, 0 = read, 1 = write)
	//Note that reads and writes are written to the addresses in little endian format
	//then converted back to be output / vise-versa for inputs
	//This makes it so for reads rd[0] = portXMemoryAddress[24] = portX[0]
	//and for writes............rs2[0] = portXMemoryAddress[24] = portX[0]
	//and for direction setting rs2[0] = portXDirMemAddress[24] = portXDirOfPin[0]
	//My recomendation is therefore that ports are accessed whole words at a time
	//but if you keep the little endian -> big endian format in mind you can write half words or bytes
	//Addresses for port read/write   |   io direction (0 for input, 1 for output)
	inout [31:0] mmPortA,//FFFFFFF0   |   FFFFFFE0
	inout [31:0] mmPortB,//FFFFFFF4   |   FFFFFFE4
	inout [31:0] mmPortC,//FFFFFFF8   |   FFFFFFE8
	inout [31:0] mmPortD,//FFFFFFFC   |   FFFFFFEC
	
	//Exposed port direction registers: Usefull for external modules to avoid multiple driver problems when compiling
	//portX[Y] can only be written to by an external moduleif it is not in output mode
	//if portXDirection[y] is 1 then that pin is outputting data, if portXDirection[y] is 0 then that pin is high impedance
	output [31:0] mmPortADirection,
	output [31:0] mmPortBDirection,
	output [31:0] mmPortCDirection,
	output [31:0] mmPortDDirection
);
/* Wires and Assignments */

wor [31:0] combinedDataOut;
assign dataOut = combinedDataOut;

wire [31:0] ramDataOut;
wire [31:0] ioDataOut;
//Wor assignment
assign combinedDataOut = ramDataOut;
assign combinedDataOut = ioDataOut;

/* Backend backends (handle bounds checking themselves) */

//Ram
RAMWrapper #(.INITIAL_RAM_CONTENTS(INITIAL_RAM_CONTENTS), .A_WIDTH(A_WIDTH)) ram
				(.clock(clock), .address(address), .dataIn(dataIn), .dataOut(ramDataOut), .writeEnable(writeEnable), .instructionOut(instructionOut), .instructionAddress(instructionAddress));

//Memory mapped IO
MemoryMappedIOManager memoryMappedIO (.clock(clock), .reset(reset), .address(address), .dataIn(dataIn), .dataOut(ioDataOut), .writeEnable(writeEnable), .mmPortA(mmPortA), .mmPortB(mmPortB),
												  .mmPortC(mmPortC), .mmPortD(mmPortD), .mmPortADirection(mmPortADirection), .mmPortBDirection(mmPortBDirection), .mmPortCDirection(mmPortCDirection),
												  .mmPortDDirection(mmPortDDirection));

/* Option For Speed */

//Replacing all of above code (except for parameters section) with this increases fmax alot
//Comes at cost of no memory mapped io though
//sync_ram #(.FILE(INITIAL_RAM_CONTENTS), .A_WIDTH(A_WIDTH), .D_WIDTH(32), .INITIALIZE_FROM_FILE(1)) temp (.clock(clock), .write_enable(writeEnable), .data_out(dataOut),
//																																			  .read_address(address), .data_in(dataIn), .write_address(address));

endmodule 
