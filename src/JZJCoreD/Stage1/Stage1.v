module Stage1//Instruction fetch//FIXME insure value passed on first posedge to decode is either nop or a valid instruction
(
	input clock,
	input reset,
	
	//Control lines
	input stall,//Don't update pc and hold output
	input annul,//Output nop but still store a new pc
	
	//Inputs
	input [31:0] instructionIn,//Little endian from memory
	input [31:0] pcIn,//New value for pc latched on posedges that this module is not stalled
	
	//Outputs
	output reg [31:0] instructionOut,//To stage 2 (decode)
	output [29:0] instructionAddress,//To memory module
	output reg [31:0] pcOut,//To stage 2 (decode)
	output pcMisaligned//To control logic
);
/* Wires and Assignments */

wire [31:0] currentPCValue;
wire pcWE = ~stall;//We always write to the pc, that was if we're stalled the pc can be updated during the stall and we can output immediatly afterwards
assign instructionAddress = pcOut[31:2];//2 bit offset is ignored because it must be 0 otherwise pc will halt cpu with pcMisaligned

/* Instruction Fetching and Output Logic */

//Memory already takes a clock cycle so that's why this is combinational
always @*
begin
	if (annul)
	begin
		instructionOut = 32'h00000013;//Nop instruction
	end
	else
	begin
		instructionOut = toBigEndian(instructionIn);//Pass the instruction through
	end
end

always @(posedge clock, posedge reset)
begin
	if (reset)
		pcOut <= 32'h00000000;
	else if (clock)
	begin
		if (~stall)//Only output a new pc value if not stalled; if annulled the pc dosen't really matter
			pcOut <= currentPCValue;
	end
end

/* External Stuff */

//Endianness functions
`include "JZJCoreD/Memory/EndiannessFunctions.v"

//Program counter
ProgramCounter programCounter (.clock(clock), .reset(reset), .writeEnable(pcWE), .newPCIn(pcIn), .pcOut(currentPCValue), .misaligned(pcMisaligned));

endmodule