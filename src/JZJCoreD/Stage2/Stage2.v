module Stage2//Decode
(
	input clock,
	input reset,
	
	//Control lines
	input stall,
	input annul,
	
	//Inputs
	input [31:0] instructionIn,
	input [31:0] pcIn,
	input [31:0] rs1In,
	input [31:0] rs2In,
	
	//Outputs
	output [4:0] rs1Address,//To register file
	output [4:0] rs2Address,//To register file
	output reg [31:0] rs1Out,//To stage 3 (execute)
	output reg [31:0] rs2Out,//To stage 3 (execute)
	output reg [31:0] immediateOut,//To stage 3 (execute)
	output reg [31:0] pcOut,//To stage 3 (execute)
	output reg [6:0] opcodeOut,//To stage 3 (execute)
	output reg [2:0] funct3Out,//To stage 3 (execute)
	output reg [6:0] funct7Out,//To stage 3 (execute)
	output reg [4:0] rdAddressOut,//To stage 3 (execute)
	output reg memoryReadFlagOut,//To stage 3 (execute)
	output reg memoryWriteFlagOut//To stage 3 (execute)
);
/* Wires and Registers */

//Direct outputs from instruction decoder
wire [6:0] funct7;
wire [2:0] funct3;
wire [4:0] rd;
wire [6:0] opcode;
wire [31:0] immediateI;
wire [31:0] immediateS;
wire [31:0] immediateB;
wire [31:0] immediateU;
wire [31:0] immediateJ;

reg [31:0] immediateToOutput;
reg memoryReadFlagToOutput;//1 if need to read from memory during stage 4
reg memoryWriteFlagToOutput;//1 if writing to memory during stage 5, 0 if writing to a register during stage 5 (even register x0)
reg [4:0] rdToOutput;

/* Output Logic */

always @(posedge clock, posedge reset)
begin
	if (reset)
		initRegisters();
	else if (clock)
	begin
		if (annul)
			initRegisters();//Output nop
		else if (~stall)
		begin
			rs1Out <= rs1In;//Address for register file set by instruction decoder module
			rs2Out <= rs2In;//Address for register file set by instruction decoder module
			immediateOut <= immediateToOutput;
			pcOut <= pcIn;
			opcodeOut <= opcode;
			funct3Out <= funct3;
			funct7Out <= funct7;
			rdAddressOut <= rdToOutput;//Todo set to 5'b00000 if not writing to a register
			memoryReadFlagOut <= memoryReadFlagToOutput;
			memoryWriteFlagOut <= memoryWriteFlagToOutput;
		end
	end
end

/* Immediate Multiplexing Logic */

always @*
begin
	case (opcode)
		7'b0110111: immediateToOutput = immediateU;//lui
		7'b0010111: immediateToOutput = immediateU;//auipc
		7'b1101111: immediateToOutput = immediateJ;//jal
		7'b1100111: immediateToOutput = immediateI;//jalr
		7'b1100011: immediateToOutput = immediateB;//branch instructions
		7'b0000011: immediateToOutput = immediateI;//memory read instructions
		7'b0100011: immediateToOutput = immediateS;//memory write instructions
		7'b0010011: immediateToOutput = immediateI;//alu immediate instructions
		default: immediateToOutput = 32'h00000000;//No immediate in this instruction format
	endcase
end

/* Memory/Register Read/Write Flag Logic */

always @*
begin
	if (opcode == 7'b0000011)//memory read instructions
	begin
		memoryReadFlagToOutput = 1'b1;//reading from memory
		memoryWriteFlagToOutput = 1'b0;//saving to a register
	end
	else if (opcode == 7'b0100011)//memory write instructions
	begin
		if (funct3 == 3'b010)//sw
		begin
			memoryReadFlagToOutput = 1'b0;//don't need to read from memory
			memoryWriteFlagToOutput = 1'b1;//writing to memory
		end
		else//sh or sb
		begin
			memoryReadFlagToOutput = 1'b1;//need to read for read modify write cycle
			memoryWriteFlagToOutput = 1'b1;//writing to memory
		end
	end
	else//any other instruction
	begin
		memoryReadFlagToOutput = 1'b0;//not reading from memory
		memoryWriteFlagToOutput = 1'b1;//Writing to rd; if the instruction does not write to memory or a register, then this should be 1 and rd should be x0
	end
end

/* RD Multiplexing Logic */

always @*
begin
	case (opcode)
		7'b1100011: rdToOutput = 5'b00000;//branch instructions
		7'b0100011: rdToOutput = 5'b00000;//memory write instructions
		default: rdToOutput = rd;//RD is in this instruction format so pass it along
	endcase
end

/* External Stuff */

InstructionDecoder instructionDecoder (.instruction(instructionIn), .funct7(funct7), .rs2(rs2Address), .rs1(rs1Address), .funct3(funct3), .rd(rd), .opcode(opcode), .immediateI(immediateI),
													.immediateS(immediateS), . immediateB(immediateB), .immediateU(immediateU), .immediateJ(immediateJ));
													
/* Register Initialization */

//Initialize registers at powerup
initial
begin
	initRegisters();
end

//Outputs nop at startup
task initRegisters();//Intentionally not automatic because the only things that use it are the initial block and the output block
begin
	rs1Out <= 32'h00000000;
	rs2Out <= 32'h00000000;
	immediateOut <= 32'h00000000;
	pcOut <= 32'h00000000;
	opcodeOut <= 7'b0010011;//Nop
	funct3Out <= 3'b000;
	funct7Out <= 7'b0000000;
	rdAddressOut <= 5'b00000;
	memoryReadFlagOut <= 1'b0;
	memoryWriteFlagOut <= 1'b0;//Writing to a register (x0)
end
endtask

endmodule 
