//Note: This is not part of the core, just a file for me to use to test the core on my devboard
//To actually use the core, just add JZJCoreD.v and its dependencies to your project (everything in the JZJCoreD folder)
module TopTestingFile//heavily borrowed from JZJCoreC
(
	input clock,//50mhz
	input notReset,

	//Testing things I have setup
	input [3:0] notButton,
	output [3:0] notLed,
	output [7:0] logicAnalyzerOut,
	//7 segment display
	output [7:0] segment,
	output [3:0] digit
);
//Inversion for inverted devboard stuff
wire reset = ~notReset;
wire [3:0] button = ~notButton;
wire [3:0] led;
assign notLed = ~led;

//Clock stuff division stuff
reg [18:0] clockPrescaler = 19'h00000;
always @(posedge clock)
begin
	clockPrescaler <= clockPrescaler + 19'h00001;
end

wire clock25MHz = clockPrescaler[0];//25mhz (50mhz / (2^1)) (just under fmax)
wire clock90Hz = clockPrescaler[18];//about 90 hz (50mhz / (2^19)) (for debugging)

//Wires
wire [31:0] register31Output;

assign logicAnalyzerOut[7] = clock90Hz;
assign logicAnalyzerOut[6:0] = register31Output;
wire [31:0] portA;
wire [31:0] portADirection;
wire [31:0] portB;
wire [31:0] portBDirection;

//Port stuffs
assign portA[0] = (portADirection[0] == 0) ? button[0] : 1'bz;
assign portA[1] = (portADirection[1] == 0) ? button[1] : 1'bz;
assign portA[2] = (portADirection[2] == 0) ? button[2] : 1'bz;
assign portA[3] = (portADirection[3] == 0) ? button[3] : 1'bz;
assign led[3:0] = portA[7:4];

wire [15:0] displayOutput;
//assign displayOutput = portB[15:0];
assign displayOutput = register31Output;

//The core
localparam FILE = "memFiles/memorymappediowritetest.mem";

//Full speed
JZJCoreD #(.INITIAL_MEM_CONTENTS(FILE)) coreTest
(.clock(clock), .reset(reset), .register31Output(register31Output), .portA(portA), .portADirection(portADirection), .portB(portB), .portBDirection(portBDirection));

//Half speed
//JZJCoreD #(.INITIAL_MEM_CONTENTS(FILE)) coreTest
//(.clock(clock25MHz), .reset(reset), .register31Output(register31Output), .portA(portA), .portADirection(portADirection), .portB(portB), .portBDirection(portBDirection));

//Slow
//JZJCoreD #(.INITIAL_MEM_CONTENTS(FILE)) coreTest
//(.clock(clock90Hz), .reset(reset), .register31Output(register31Output), .portA(portA), .portADirection(portADirection), .portB(portB), .portBDirection(portBDirection));

//7 segment display output
multi7seg (.clock(clockPrescaler[17]), .data0(displayOutput[15:12]), .data1(displayOutput[11:8]), .data2(displayOutput[7:4]), .data3(displayOutput[3:0]), .segment(segment), .ground(digit));

endmodule